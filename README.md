# MeDiktál Speech Recognition

This repository 2 parts: 

1) The index.html starts the microfon in the browser, and streams the data via websocket to the backend server.

2) The python part of the application receives the audio stream from the web app (or from the client python application -> pelda_client.py), 
and sends the data to the Google Cloud Speech-to-text API. Then it also receives the data from the API as a string stream, and sends it to the web application to
show the typed text converted from the speech.

In order to run the project:

1 - get a google cloud license key, for this step, follow the instructions: https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries

2 - create a virtual environment by installing the conda from environment.yml

3 - run `chanel_socket_io.py`
